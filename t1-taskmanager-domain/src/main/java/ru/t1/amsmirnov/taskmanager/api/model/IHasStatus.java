package ru.t1.amsmirnov.taskmanager.api.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
