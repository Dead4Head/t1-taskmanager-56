package ru.t1.amsmirnov.taskmanager.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.api.endpoint.*;
import ru.t1.amsmirnov.taskmanager.api.repository.ICommandRepository;
import ru.t1.amsmirnov.taskmanager.api.service.*;
import ru.t1.amsmirnov.taskmanager.command.AbstractCommand;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.system.ArgumentNotSupportedException;
import ru.t1.amsmirnov.taskmanager.exception.system.CommandNotSupportedException;
import ru.t1.amsmirnov.taskmanager.repository.CommandRepository;
import ru.t1.amsmirnov.taskmanager.service.*;
import ru.t1.amsmirnov.taskmanager.util.SystemUtil;
import ru.t1.amsmirnov.taskmanager.util.TerminalUtil;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.t1.amsmirnov.taskmanager.command";

    @NotNull
    @Autowired
    private ICommandRepository commandRepository;

    @Getter
    @NotNull
    @Autowired
    private ICommandService commandService;

    @Getter
    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Getter
    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @Getter
    @NotNull
    @Autowired
    private ITokenService tokenService;

    @NotNull
    @Autowired
    private IAuthEndpoint authEndpoint;

    @NotNull
    @Autowired
    private ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    private IDomainEndpoint domainEndpoint;

    @NotNull
    @Autowired
    private IProjectEndpoint projectEndpoint;

    @NotNull
    @Autowired
    private ITaskEndpoint taskEndpoint;

    @NotNull
    @Autowired
    private IUserEndpoint userEndpoint;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @Nullable
    @Autowired
    private AbstractCommand[]  abstractCommands;

    private void initApplication() {
        try {
            initPID();
            loggerService.info("** WELCOME TO TASK MANAGER **");
            Runtime.getRuntime().addShutdownHook(new Thread(this::shutdownApplication));
            registry(abstractCommands);
            fileScanner.start();
        } catch (final Exception exception) {
            renderError(exception);
        }
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        @NotNull final String fileName = "taskmanager(" + pid + ").pid";
        Files.write(Paths.get(fileName), pid.getBytes());
        @NotNull final File file = new File(fileName);
        file.deleteOnExit();
    }


    private void shutdownApplication() {
        loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    private void renderError(@NotNull Exception exception) {
        loggerService.error(exception);
        System.out.println("[FAIL]");
    }

    public void start(@Nullable String[] args) {
        if (processArguments(args)) exit();
        initApplication();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND: ");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (@NotNull final Exception exception) {
                renderError(exception);
            }
        }
    }

    private void registry(@NotNull final AbstractCommand[] commands) {
        for (@Nullable final AbstractCommand command : commands) {
            if (command != null)
                commandService.add(command);
        }
    }

    public boolean processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String argument = args[0];
        try {
            processArgument(argument);
        } catch (@NotNull final Exception exception) {
            renderError(exception);
        }
        return true;
    }

    public void processArgument(@Nullable final String argument) throws AbstractException {
        @Nullable final AbstractCommand command = commandService.getCommandByArgument(argument);
        if (command == null) throw new ArgumentNotSupportedException(argument);
        command.execute();
    }

    public void processCommand(@NotNull final String commandName) throws AbstractException {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(commandName);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        abstractCommand.execute();
    }

    public void exit() {
        System.exit(0);
    }

    @NotNull
    @Override
    public IAuthEndpoint getAuthEndpoint() {
        return authEndpoint;
    }

    @NotNull
    @Override
    public ISystemEndpoint getSystemEndpoint() {
        return systemEndpoint;
    }

    @NotNull
    @Override
    public IDomainEndpoint getDomainEndpoint() {
        return domainEndpoint;
    }

    @NotNull
    @Override
    public IProjectEndpoint getProjectEndpoint() {
        return projectEndpoint;
    }

    @NotNull
    @Override
    public ITaskEndpoint getTaskEndpoint() {
        return taskEndpoint;
    }

    @NotNull
    @Override
    public IUserEndpoint getUserEndpoint() {
        return userEndpoint;
    }

}
