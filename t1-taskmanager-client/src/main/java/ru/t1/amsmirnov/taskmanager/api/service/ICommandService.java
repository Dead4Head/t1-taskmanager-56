package ru.t1.amsmirnov.taskmanager.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.amsmirnov.taskmanager.command.AbstractCommand;

import java.util.Collection;

@Service
public interface ICommandService {

    void add(@Nullable AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable String argument);

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

    @NotNull
    Iterable<AbstractCommand> getCommandWithArgument();

}
