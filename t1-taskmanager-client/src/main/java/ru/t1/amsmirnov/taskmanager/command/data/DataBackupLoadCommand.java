package ru.t1.amsmirnov.taskmanager.command.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.data.DataBase64LoadRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.data.DataBase64LoadResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

@Component
public final class DataBackupLoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "backup-load";

    @NotNull
    public static final String DESCRIPTION = "Load data from backup file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[BACKUP DATA LOAD]");
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest(getToken());
        @NotNull final DataBase64LoadResponse response = domainEndpoint.loadDataBase64(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
