package ru.t1.amsmirnov.taskmanager.command.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import ru.t1.amsmirnov.taskmanager.dto.request.project.ProjectClearRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.project.ProjectClearResponse;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.CommandException;

@Component
public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    public static final String NAME = "project-clear";

    @NotNull
    public static final String DESCRIPTION = "Clear project list.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CLEAR PROJECTS LIST]");
        @NotNull final ProjectClearRequest request = new ProjectClearRequest(getToken());
        @NotNull final ProjectClearResponse response = projectEndpoint.removeAllProjects(request);
        if (!response.isSuccess())
            throw new CommandException(response.getMessage());
    }

}
