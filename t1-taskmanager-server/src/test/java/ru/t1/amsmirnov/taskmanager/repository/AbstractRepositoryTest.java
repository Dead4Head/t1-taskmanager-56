//package ru.t1.amsmirnov.taskmanager.repository;
//
//import liquibase.Liquibase;
//import org.jetbrains.annotations.NotNull;
//import org.junit.AfterClass;
//import org.junit.BeforeClass;
//import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
//import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;
//import ru.t1.amsmirnov.taskmanager.migration.AbstractSchemeTest;
//import ru.t1.amsmirnov.taskmanager.service.PropertyService;
//
//import javax.persistence.EntityManager;
//import javax.persistence.EntityTransaction;
//
//public abstract class AbstractRepositoryTest extends AbstractSchemeTest {
//
//    @NotNull
//    protected static final IPropertyService PROPERTY_SERVICE = new PropertyService();
//
//    @NotNull
//    protected static final IConnectionService CONNECTION_SERVICE = new ConnectionService(PROPERTY_SERVICE);
//
//    protected static final int NUMBER_OF_ENTRIES = 10;
//
//    protected static final String NONE_ID = "---NONE---";
//
//    @NotNull
//    protected static EntityManager ENTITY_MANAGER;
//
//    @NotNull
//    protected static EntityTransaction TRANSACTION;
//
//    @BeforeClass
//    public static void initDataBase() throws Exception {
//        final Liquibase liquibase = CONNECTION_SERVICE.getLiquibase();
//        liquibase.dropAll();
//        liquibase.update("scheme");
//
//        ENTITY_MANAGER = CONNECTION_SERVICE.getEntityManager();
//        TRANSACTION = ENTITY_MANAGER.getTransaction();
//    }
//
//    @AfterClass
//    public static void closeConnection() {
//        ENTITY_MANAGER.close();
//    }
//
//}
