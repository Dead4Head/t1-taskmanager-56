package ru.t1.amsmirnov.taskmanager.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.ITaskDtoRepository;
import ru.t1.amsmirnov.taskmanager.api.service.dto.IProjectDtoService;
import ru.t1.amsmirnov.taskmanager.api.service.dto.IProjectTaskDtoService;
import ru.t1.amsmirnov.taskmanager.dto.model.TaskDTO;
import ru.t1.amsmirnov.taskmanager.exception.AbstractException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ModelNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.ProjectNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.entity.TaskNotFoundException;
import ru.t1.amsmirnov.taskmanager.exception.field.ProjectIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.TaskIdEmptyException;
import ru.t1.amsmirnov.taskmanager.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import java.util.List;

@Service
public final class ProjectTaskDtoService implements IProjectTaskDtoService {

    @NotNull
    @Autowired
    private ApplicationContext context;

    public ProjectTaskDtoService() {
    }

    @Override
    public void bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final ITaskDtoRepository taskRepository = context.getBean(ITaskDtoRepository.class);
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        @NotNull final IProjectDtoService projectService = context.getBean(IProjectDtoService.class);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (!projectService.existById(userId, projectId)) throw new ProjectNotFoundException();
            final TaskDTO task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(projectId);
            taskRepository.update(task);
            transaction.commit();
        } catch (final Exception exception) {
            transaction.rollback();
            throw exception;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @NotNull final ITaskDtoRepository taskRepository = context.getBean(ITaskDtoRepository.class);
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        @NotNull final IProjectDtoService projectService = context.getBean(IProjectDtoService.class);
        try {
            transaction.begin();
            if (!projectService.existById(userId, projectId)) throw new ProjectNotFoundException();
            final List<TaskDTO> tasks = taskRepository.findAllByProjectId(userId, projectId);
            if (tasks == null) throw new ModelNotFoundException("TaskList");
            taskRepository.removeAll(userId, projectId);
            projectService.removeOneById(userId, projectId);
            transaction.commit();
        } catch (final Exception exception) {
            transaction.rollback();
            throw exception;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @NotNull final ITaskDtoRepository taskRepository = context.getBean(ITaskDtoRepository.class);
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        @NotNull final IProjectDtoService projectService = context.getBean(IProjectDtoService.class);
        @NotNull final EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            if (!projectService.existById(userId, projectId)) throw new ProjectNotFoundException();
            final TaskDTO task = taskRepository.findOneById(userId, taskId);
            if (task == null) throw new TaskNotFoundException();
            task.setProjectId(null);
            taskRepository.update(task);
            transaction.commit();
        } catch (final Exception exception) {
            transaction.rollback();
            throw exception;
        } finally {
            entityManager.close();
        }
    }

}
