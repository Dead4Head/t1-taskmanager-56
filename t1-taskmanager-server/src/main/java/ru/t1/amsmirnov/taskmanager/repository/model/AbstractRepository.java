package ru.t1.amsmirnov.taskmanager.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.amsmirnov.taskmanager.api.repository.model.IAbstractRepository;
import ru.t1.amsmirnov.taskmanager.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Collection;


@Repository
@Scope("prototype")
public abstract class AbstractRepository<M extends AbstractModel> implements IAbstractRepository<M> {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    public AbstractRepository() {

    }

    @NotNull
    @Override
    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void addAll(@NotNull final Collection<M> models) {
        models.forEach(this::add);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

}
