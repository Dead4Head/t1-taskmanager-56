package ru.t1.amsmirnov.taskmanager.endpoint;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.amsmirnov.taskmanager.api.endpoint.ISystemEndpoint;
import ru.t1.amsmirnov.taskmanager.api.service.IConnectionService;
import ru.t1.amsmirnov.taskmanager.api.service.IPropertyService;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerAboutRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerDropSchemeRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerUpdateSchemeRequest;
import ru.t1.amsmirnov.taskmanager.dto.request.server.ServerVersionRequest;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerAboutResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerDropSchemeResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerUpdateSchemeResponse;
import ru.t1.amsmirnov.taskmanager.dto.response.server.ServerVersionResponse;
import ru.t1.amsmirnov.taskmanager.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.t1.amsmirnov.taskmanager.api.endpoint.ISystemEndpoint")
public final class SystemEndpoint extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IConnectionService connectionService;

    public SystemEndpoint() {
        super();
    }

    @NotNull
    @Override
    @WebMethod
    public ServerAboutResponse getAbout(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerAboutRequest serverAboutRequest
    ) {
        @NotNull final ServerAboutResponse serverAboutResponse = new ServerAboutResponse();
        serverAboutResponse.setEmail(propertyService.getAuthorEmail());
        serverAboutResponse.setName(propertyService.getAuthorName());
        return serverAboutResponse;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerVersionResponse getVersion(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerVersionRequest serverVersionRequest
    ) {
        @NotNull final ServerVersionResponse serverVersionResponse = new ServerVersionResponse();
        serverVersionResponse.setVersion(propertyService.getApplicationVersion());
        return serverVersionResponse;
    }

    @NotNull
    @Override
    @WebMethod
    public ServerUpdateSchemeResponse updateScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerUpdateSchemeRequest request
    ) {
        try {
            //check(request, Role.ADMIN);
            @NotNull final Liquibase liquibase = connectionService.getLiquibase();
            liquibase.update(request.getScheme());
            return new ServerUpdateSchemeResponse();
        } catch (final Exception e) {
            return new ServerUpdateSchemeResponse(e);
        }
    }

    @NotNull
    @Override
    @WebMethod
    public ServerDropSchemeResponse dropScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final ServerDropSchemeRequest request
    ) {
        try {
            check(request, Role.ADMIN);
            @NotNull final Liquibase liquibase = connectionService.getLiquibase();
            liquibase.dropAll();
            return new ServerDropSchemeResponse();
        } catch (final Exception e) {
            return new ServerDropSchemeResponse(e);
        }
    }

}
