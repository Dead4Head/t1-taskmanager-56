package ru.t1.amsmirnov.taskmanager.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.amsmirnov.taskmanager.api.repository.dto.IAbstractUserOwnedDtoRepository;
import ru.t1.amsmirnov.taskmanager.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;

@Repository
@Scope("prototype")
public abstract class AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO>
        extends AbstractDtoRepository<M>
        implements IAbstractUserOwnedDtoRepository<M> {

    public AbstractUserOwnedDtoRepository() {
        super();
    }

}
